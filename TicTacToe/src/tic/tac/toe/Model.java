/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tic.tac.toe;

/**
 *
 * @author hp-
 */
public class Model {
    
    String[][] matrix = {{" ", " ", " "}, {" ", " ", " "}, {" ", " ", " "}};
    
    public boolean organizingUserInput(int[] input, int i)
    {
        boolean result = false;
        int row = input[0];
        int column = input[1];
        if(matrix[row][column] == " ")
        {
        if(i % 2 == 0)
        {
            matrix[row][column] = "O";
        }
        if(i % 2 != 0)
        {
            matrix[row][column] = "X";
        }
        result = true;
        }
        else
        {
        i--;
        result = false;
        }
        return result;
    }
    public boolean winning(String[][] matrix)
    {
        boolean isTrue = false;
        int Xcount=0, Ocount = 0;
        int j=0;
      if(matrix[0][0] == "X" && matrix[1][1] == "X" && matrix[2][2] == "X"){isTrue = true;}
        
      if(matrix[0][0] == "O" && matrix[1][1] == "O" && matrix[2][2] == "O"){isTrue = true;}
        
      for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                if(matrix[i][k] == "X"){Xcount+=1;}
                if(matrix[i][k] == "O"){Ocount+=1;}
                if(Xcount == 3){isTrue = true;}
                if(Ocount == 3){isTrue = true;}  
            }
            Xcount=0; Ocount = 0; 
        }
       for (int i = 0; i < matrix[0].length; i++) {
            for (int k = 0; k < matrix.length; k++) {
                if(matrix[k][i] == "X"){Xcount+=1;}
                if(matrix[k][i] == "O"){Ocount+=1;}
                if(Xcount == 3){isTrue = true;}
                if(Ocount == 3){isTrue = true;}  
            }
             Xcount=0; Ocount = 0; 
        }
      return isTrue;  
    }
}
    

