/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tic.tac.toe;

/**
 *
 * @author hp-
 */
public class Controller {
    
    private Model model;
    private View view;
    
    public Controller(Model model, View view)
    {
        this.model = model;
        this.view = view;
    }
    
    public void DisplayingTable(String[][] matrix)
    {
        view.DisplayTable(matrix);
    }
    public boolean bool(String[][] v)
    {
        return model.winning(v);
    }
    public boolean validity(int[] input, int i)
    {
        return model.organizingUserInput(input, i);
    }
    public void finish(boolean b, String[][] matrix)
    {
        if(bool(matrix)){DisplayingTable(matrix);}
        view.play(b);
        
    }
}
