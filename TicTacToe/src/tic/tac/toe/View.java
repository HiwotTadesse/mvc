/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tic.tac.toe;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author hp-
 */
public class View {
    
    int counter = 0;
    public void DisplayTable(String[][] matrix)
    {
        System.out.println("-------");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print("|" + matrix[i][j]);
            }
            System.out.println("|");
            System.out.println("-------");
            
        }
    }
    public int[] TakingUserInput()
    {
        int column =0,row=0;
        if(counter % 2 == 0)
        {
        System.out.println("Enter a row (0, 1, 0r 2) for player X: ");
        Scanner sc = new Scanner(System.in);
        row = sc.nextInt();
        System.out.println("Enter a column (0, 1, 0r 2) for player X: ");
        column = sc.nextInt();
        }
        
        if(counter % 2 != 0)
        {
        System.out.println("Enter a row (0, 1, 0r 2) for player O: ");
        Scanner sc = new Scanner(System.in);
        row = sc.nextInt();
        System.out.println("Enter a column (0, 1, 0r 2) for player O: ");
        column = sc.nextInt();

        }
                counter+=1;
        int[] input = new int[2];
        input[0] = row;input[1] = column;
        return input;
    }
    public void play(boolean isTrue)
    {
        if(counter % 2 != 0 && isTrue){System.out.println("X Player won ...Congrats....:):)");}
        if(counter % 2 == 0 && isTrue){System.out.println("O Player won ...Congrats....:):)");}
        
    }
    public void Invalid(boolean isTrue)
    {
      if(!isTrue)
      {
          System.out.println("Invalid Input");
      }
    }
}
